/*
* protocol.c:  Functions for sending RFC 2812 client messages/commands to the server
*/

#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/fcntl.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
#include "irclib.h"
#include "types.h"

int	ClientSendPASS(int comsocket, char *password)
{
	char buffer[64];
	
	sprintf(buffer, "PASS %s\r\n", password);
	sputline(comsocket, buffer);
}

int     ClientSendNICK(int comsocket, char *nickname)
{
        char buffer[1024];
        char getbuffer[1024];
        int i = 0;

        sprintf(buffer, "NICK %s\r\n", nickname);

        sputline(comsocket, buffer);
}
	
int	ClientSendUSER(int comsocket, char *user, char *mode, char *unused, char *realname)
{
	char buffer[100];

	sprintf(buffer, "USER %s %s %s :%s\r\n", user, mode, unused, realname);
	sputline(comsocket, buffer);
}

int	ClientSendOPER(int comsocket, char *name, char *password)
{
	char buffer[64];
	
	sprintf(buffer, "OPER %s %s\r\n", name, password);
	sputline(comsocket, buffer);
}

int	ClientSendUserMODE(int comsocket, char *nick, char *mode)
{
	char buffer[64];

	sprintf(buffer, "MODE %s %s\r\n", nick, mode);
	sputline(comsocket, buffer);
}

int	ClientSendSERVICE(int comsocket, char *nickname, char *reserved, char *distribution, char *type, char *reserved2, char *info)
{
	char buffer[200];

	sprintf(buffer, "SERVICE %s %s %s %s %s %s\r\n", nickname, reserved, distribution, type, reserved2, info);
	sputline(comsocket, buffer);
}
 
int	ClientSendQUIT(int comsocket, char *message)
{
	char buffer[200];

	sprintf(buffer, "QUIT :%s\r\n", message);
	sputline(comsocket, buffer);
}

int	ClientSendSQUIT(int comsocket, char *server, char *comment)
{
	char buffer[200];

	sprintf(buffer, "SQUIT %s :%s\r\n", server, comment);
	sputline(comsocket, buffer);
}
 
int	ClientSendPART(int comsocket, char *channel, char *partmessage)
{
	char buffer[200];

	sprintf(buffer, "PART %s :%s\r\n", channel, partmessage);
	sputline(comsocket, buffer);
}

int	ClientSendTOPIC(int comsocket, char *channel, char *topic)
{
	char buffer[200];

	sprintf(buffer, "TOPIC %s :%s\r\n", channel, topic);
	sputline(comsocket, buffer);
}

int	ClientSendNAMES(int comsocket, char *channel, char *target)
{
	char buffer[200];

	sprintf(buffer, "NAMES %s %s\r\n", channel, target);
	sputline(comsocket, buffer);
}

int	ClientSendLIST(int comsocket, char *channel, char *target)
{
	char buffer[200];
	
	sprintf(buffer, "LIST %s %s\r\n", channel, target);
	sputline(comsocket, buffer);
}

int	ClientSendINVITE(int comsocket, char *nick, char *channel)
{
	char buffer[100];

	sprintf(buffer, "INVITE %s %s\r\n", nick, channel);
	sputline(comsocket, buffer);
}

int	ClientSendKICK(int comsocket, char *channel, char *nick, char *comment)
{
	char buffer[100];

	sprintf(buffer, "KICK %s %s :%s\r\n", channel, nick, comment);
	sputline(comsocket, buffer);
}

int	ClientSendNOTICE(int comsocket, char *target, char *message)
{
	char buffer[200];

	sprintf(buffer, "NOTICE %s :%s\r\n", target, message);
	sputline(comsocket, buffer);
}

int     ClientSendMOTD(int comsocket, char *server)
{
        char buffer[64];

        sprintf(buffer, "MOTD %s\r\n", server);
        sputline(comsocket, buffer);
}

int	ClientSendLUSERS(int comsocket, char *mask, char *server)
{
	char buffer[64];

	sprintf(buffer, "LUSERS %s %s\r\n", mask, server);
	sputline(comsocket, buffer);
}

int	ClientSendVERSION(int comsocket, char *server)
{
	char buffer[64];

	sprintf(buffer, "VERSION %s\r\n", server);
	sputline(comsocket, buffer);
}

int	ClientSendSTATS(int comsocket, char *query, char *server)
{
	char buffer[64];

	sprintf(buffer, "STATS %s %s\r\n", query, server);
	sputline(comsocket, buffer);
}

int	ClientSendLINKS(int comsocket, char *remoteserver, char *servermask)
{
	char buffer[100];

	sprintf(buffer, "LINKS %s %s\r\n", remoteserver, servermask);
	sputline(comsocket, buffer);
}

int	ClientSendTIME(int comsocket, char *server)
{
	char buffer[100];

	sprintf(buffer, "TIME %s\r\n", server);
	sputline(comsocket, buffer);
}

/*
 * ClientSendCONNECT
 * Sends a connect request from one server to the another 
 */

int	ClientSendCONNECT(int comsocket, char *server, char *port, char *remoteserver)
{
	char buffer [200];

	sprintf(buffer, "CONNECT %s %s %s\r\n", server, port, remoteserver);
	sputline(comsocket, buffer);
}

int	ClientSendTRACE(int comsocket, char *server)
{
	char buffer[200];
	
	sprintf(buffer, "TRACE %s\r\n", server);
	sputline(comsocket, buffer);
}

int	ClientSendADMIN(int comsocket, char *server)
{
	char buffer[100];
	
	sprintf(buffer,"ADMIN %s\r\n", server);
	sputline(comsocket, buffer);
}

int     ClientSendINFO(int comsocket, char *server)
{
        char buffer[100];

        sprintf(buffer,"INFO %s\r\n", server);
        sputline(comsocket, buffer);
}

int	ClientSendSERVLIST(int comsocket, char *mask, char *type)
{
	char buffer[100];

	sprintf(buffer, "SERVLIST %s %s\r\n", mask, type);
	sputline(comsocket, buffer);
}

int	ClientSendSQUERY(int comsocket, char *servicename, char *message)
{
	char buffer[100];

	sprintf(buffer, "SQUERY %s :%s\r\n", servicename, message);
	sputline(comsocket, buffer);
}

int	ClientSendWHO(int comsocket, char *mask, char *operchar)
{
	char buffer[100];

	sprintf(buffer, "WHO %s %s\r\n", mask, operchar);
	sputline(comsocket, buffer);
}

int	ClientSendWHOIS(int comsocket, char *target, char *mask)
{
	char	buffer[64];
	char	*nick;
	char	*user;
	char	*host;

	sprintf(buffer, "WHOIS %s %s\r\n", target, mask);
	sputline(comsocket, buffer);

}

int	ClientSendWHOWAS(int comsocket, char *nickname, char *count, char *target)
{
	char buffer[64];

	sprintf(buffer, "WHOWAS %s %s %s\r\n", nickname, count, target);
	sputline(comsocket, buffer);
}

int     ClientSendKILL(int comsocket, char *nickname, char *message)
{
        char buffer[200];

        sprintf(buffer, "KILL %s %s\r\n", nickname, message);
        sputline(comsocket, buffer);
}

int	ClientSendPING(int comsocket, char *server, char *server2)
{
	char buffer[64];

	sprintf(buffer, "PING %s %s\r\n", server, server2);
	sputline(comsocket, buffer);
}

int     ClientSendPONG(int comsocket, char *epochtime)
{
        char buffer[64]="";
	
	sprintf(buffer, "PONG %s\r\n", epochtime);
	sputline(comsocket, buffer);
}

int	ClientSendAWAY(int comsocket, char *message)
{
	char buffer[64];

	sprintf(buffer, "AWAY :%s\r\n", message);
	sputline(comsocket, buffer);
}

int	ClientSendREHASH(int comsocket)
{
	char buffer[10];

	sprintf(buffer, "REHASH\r\n");
	sputline(comsocket, buffer);
}

int	ClientSendDIE(int comsocket)
{
	char buffer[10];

	sprintf(buffer, "DIE\r\n");
	sputline(comsocket, buffer);
}

int	ClientSendRESTART(int comsocket)
{
	char buffer[10];

	sprintf(buffer, "RESTART\r\n");
	sputline(comsocket, buffer);
}

int	ClientSendSUMMON(int comsocket, char *user, char *target, char *channel)
{
	char buffer[100];

	sprintf(buffer, "SUMMON %s %s %s\r\n", user, target, channel);
	sputline(comsocket, buffer);
}

int	ClientSendUSERS(int comsocket, char *server)
{
	char buffer[100];

	sprintf(buffer, "USERS %s\r\n", server);
	sputline(comsocket, buffer);
}

int	ClientSendWALLOPS(int comsocket, char *message)
{
	char buffer[255];

	sprintf(buffer, "WALLOPS :%s\r\n", message);
	sputline(comsocket, buffer);
}

int	ClientSendUSERHOST(int comsocket, char *nicknames)
{
	char buffer[255];

	sprintf(buffer, "USERHOST %s\r\n", nicknames);
	sputline(comsocket, buffer);
}

int	ClientSendISON(int comsocket, char *nicknames)
{
	char buffer[255];

	sprintf(buffer, "ISON %s\r\n", nicknames);
	sputline(comsocket, buffer);
}
	
int	ClientSendPRIVMSG(int comsocket, char *to, char *message)
{
	char buffer[255];

	sprintf(buffer, "PRIVMSG %s :%s\r\n", to, message);
	sputline(comsocket, buffer);
}

int     ClientJoinChannel(int comsocket, char *channel, char *key)

{
        char buffer[100];

        sprintf(buffer, "JOIN %s %s\r\n", channel, key);
        sputline(comsocket, buffer);
}

int	ClientSetChannelMode(int comsocket, char *channel, char *mode, char *params)
{
	char buffer[100];

	sprintf(buffer, "MODE %s %s %s\r\n", channel, mode, params);
	sputline(comsocket, buffer);
}
