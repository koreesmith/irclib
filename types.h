/* Data Types for IRCLib */

#include <stdlib.h>
#include <string.h>
#include "irclib.h"



struct newnick_type
{
        char from[50];
	char command[10];
        char nick[15];
        char hops[4];
        char time[30];
        char username[50];
        char host[50];
        char server[50];
	char realname[50];
};

struct ClientMessage 
{
	char prefix[1024];
	char command[1024];
	char params[2048];
}; 

struct ClientInfo
{
	char	nickname[30];
	char	channels[255];
	char	hostmask[255];	
	char	realname[255];
	int	IsIRCOper;
};

struct	Message001Info
{
	char	To[30];
	char	Message[255];
	char	Hostmask[100];
};

struct Message002Info
{
	char	To[30];
	char	Servername[100];
	char	Version[50];
};

struct	Message003Info
{
	char	To[30];
	char	CreationDate[50];
};

struct	Message004Info
{
	char	To[30];
	char	ServerName[100];
	char	ServerVersion[50];
	char	AvailableUserModes[50];
	char	AvailableChannelModes[50];
};

struct	Message005Info
{
	char	To[30];
	char	ServerName[100];
	char	PortNumber[7];
	char	ServerConfig[1024];
};

struct	Message301Info
{
	char	To[30];
	char	Nickname[50];
	char	AwayMessage[50];
};

struct	Message302Info
{
	char	To[30];
	char	Nickname[50];
	int	IsIRCOper;
	int	IsAway;
	char	Hostmask[255];
};
 
struct	Message303Info
{
	char	To[30];
	char	Nickname[50];
};

struct	Message305Info
{
	char	To[30];
	char	Reply[50];
};

struct Message306Info
{
	char	To[30];
	char	Reply[50];
};

struct	Message311Info
{
	char	To[30];
	char	Nickname[50];
	char	Username[50];
	char	Hostname[100];
	char	Realname[255];
};

struct	Message312Info
{
	char	To[30];
	char	Nickname[30];
	char	ServerName[255];
	char	ServerInfo[255];
};

struct	Message313Info
{
	char	To[30];
	char	Nickname[50];
	char	Reply[50];
};

struct	Message317Info
{
	char	To[50];
	char	Nickname[50];
	int	SecondsIdle;
	char	Reply[255];
};

struct	Message318Info
{
	char	To[50];
	char	Nickname[50];
	char	Reply[255];
};

struct	Message319Info
{
	char	To[59];
	char	Nickname[50];
	char	ChannelList[500];
};

struct	Message314Info
{
	char	To[50];
	char	Nickname[50];
	char	Username[50];
	char	Hostname[100];
	char	Realname[200];
};

struct	Message315Info
{
    char    To[100];
	char	Nickname[50];
    char    Message[100];
};

struct  Message322Info
{       
	    char	To[100];
        char    Channel[50];
        int     VisibleUsers;
        char    Topic[512];
};

struct  Message323Info
{
    char    To[100];
    char    Message[100];
};

struct	Message369Info
{
	char	To[50];
	char	Nickname[50];
};

struct	Message324Info
{
    char    To[50];
	char	Channel[50];
	char	Mode[20];
	char	ModeParams[50];
};
	
struct	Message325Info
{
    char    To[50];
	char	Channel[50];
	char	Nickname[50];
};

struct  Message331Info
{
    char    To[50];
    char    Channel[50];
    char    Message[50];
};

struct	Message332Info
{
    char    To[50];
	char	Channel[50];
	char	Topic[100];
};

struct	Message341Info
{
    char    To[50];
	char	Channel[50];
	char	Nickname[50];
};

struct	Message342Info
{
    char    To[50];
	char	Nickname[50];
    char    Message[100];
};

struct	Message346Info
{
    char    To[50];
	char	Channel[50];
	char	InviteMask[255];
};

struct  Message347Info
{
    char    To[50];
    char    Channel[50];
    char    Message[255];
};

struct	Message348Info
{
    char    To[50];
	char	Channel[50];
	char	ExceptionMask[1024];
};

struct  Message349Info
{
    char    To[50];
    char    Channel[50];
    char    Message[1024];
};

struct	Message351Info
{
    char    To[50];
	char	Version[50];
	char	Server[100];
	char	Comments[255];
};

struct	Message352Info
{
    char    To[50];
	char	Channel[50];
	char	User[50];
	char	Host[50];
	char	Server[100];
	char	Nickname[50];
    char    Stats[50];
	char	HopCount[10];
	char	RealName[100];
};

struct	Message353Info
{
    char    To[50];
	int	    IsSecret;
	int	    IsPrivate;
	int	    IsPublic;
	char	Channel[50];;
	char	Nicknames[8096];
};

struct	Message364Info
{
    char    To[50];
	char	Mask[100];
	char	Server[100];
	int     HopCount;
	char	ServerInfo[255];
};

struct	Message365Info
{
    char    To[100];
	char	Mask[100];
    char    Message[100];
};

struct Message366Info
{
    char    To[50];
    char    Channel[50];
    char    Message[255];
};

struct	Message367Info
{
    char    To[50];
	char	Channel[50];
	char	BanMask[200];
    char    By[50];
    char    TimeStamp[15];
};

struct	Message368Info
{
    char    To[100];
	char	Channel[50];
    char    Message[100];
};

struct	Message371Info
{
    char    To[100];
	char	Message[255];
};

struct	Message372Info
{
	char	MOTD[100];
};

struct	Message374Info
{
	char	To[100];
	char	Message[100];
};

struct	Message375Info
{
	char	To[100];
	char	Message[100];
};

struct	Message391Info
{
	char	Server[100];
	char	TimeString[50];
};

struct	Message393Info
{
	char	Username[50];
	char	ttyline[10];
	char	Hostname[100];
};



