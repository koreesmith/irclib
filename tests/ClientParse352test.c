/*
 lientParse352() Nickname Test:");
            if(strncmp(MessageInfo->Nickname, nick, sizeof(MessageInfo->Nickname)) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else  {
                printf(RED "        FAIL\n" RESET);
            }

 */

//:irc.koree.net 352 IRClib * ~irclib dev.ameth.org *.undernet.org IRClib H :0 IRC LIB
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../irclib.h"
#include "../types.h"
#include "colors.h"

void main() {
    int comsocket;
    int i;
    int tmphops;

    char buffer[255];
    char response[255];
    char server[255];
    char nick[255];
    char user[255];
    char realname[255];

    char *prefix;
    char *command;
    char *params;
    char *trailing;

    struct ClientMessage *MyMessage;
    struct  Message352Info *MessageInfo;

    strncpy(server, "irc.koree.net", sizeof(server));
    strncpy(nick, "IRClib", sizeof(nick));
    strncpy(user, "irclib", sizeof(user));
    strncpy(realname, "IRC LIB", sizeof(realname));

    comsocket = connect_to_server("irc.ameth.org", 6667);
    ClientSignOn(comsocket, user, nick, realname);

    i = 0;
    while(i == 0)  {
        sgetline(comsocket, buffer);
        MyMessage = ClientParseMessage(buffer);

        ClientSendWHO(comsocket, nick, "");
        if(strncmp(MyMessage->command, "352", 3) == 0)  {
            MessageInfo = (struct Message352Info *)malloc(sizeof(struct Message352Info));
            Mytrimcrlf(MyMessage->params);
            ClientParse352(MyMessage->prefix, MyMessage->command, MyMessage->params, MessageInfo);

            printf(BOLDYELLOW "ClientParse352() Tests\n" RESET);

            printf("    ClientParse352() To Test:");
            if(strncmp(MessageInfo->To, nick, sizeof(MessageInfo->To)) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else
            {
                printf(RED "        FAIL\n" RESET);
            }

            printf("    ClientParse352() User Test:");
            if(strncmp(MessageInfo->User, "~", 1) == 0)  {
                memmove(MessageInfo->User, MessageInfo->User+1, strlen(MessageInfo->User));
            }
            if(strncmp(MessageInfo->User, user, sizeof(MessageInfo->User)) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else  { 
                printf(RED "        FAIL\n" RESET);
            }

            printf("    ClientParse352() Host  Test:");
            if(strncmp(MessageInfo->Host, "dev.ameth.org", sizeof(MessageInfo->Host)) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else  {
                printf(RED "        FAIL\n" RESET);
            }

            printf("    ClientParse352() Server Test:");
            if(strncmp(MessageInfo->Server, "*.undernet.org", sizeof(MessageInfo->Server)) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else  {
                printf(RED "        FAIL\n" RESET);
            }

            printf("    ClientParse352() Nickname Test:");
            if(strncmp(MessageInfo->Nickname, nick, sizeof(MessageInfo->Nickname)) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else  {
                printf(RED "        FAIL\n" RESET);
            }

            printf("    ClientParse352() Stats Test:");
            if(strncmp(MessageInfo->Stats, "H", sizeof(MessageInfo->Stats)) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else  {
                printf(RED "        FAIL\n" RESET);
            }

            printf("    ClientParse352() HopCount Test:");
            tmphops = atoi(MessageInfo->HopCount);
            if(tmphops < 20 || tmphops >= 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else  {
                printf(RED "        FAIL\n" RESET);
            }


            printf("    ClientParse352() RealName Test:");
            if(strncmp(MessageInfo->RealName, realname, sizeof(MessageInfo->RealName)) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else  {
                printf(RED "        FAIL\n" RESET);
            }

            free(MessageInfo);
            exit(0);
        }
    }

}
