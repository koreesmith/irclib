/*
 * Automated test for ClientParse368() function
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../irclib.h"
#include "../types.h"
#include "colors.h"

void main() {
    int comsocket;
    int i;

    char buffer[255];
    char response[255];
    char server[255];
    char nick[255];
    char channel[255];

    char *prefix;
    char *command;
    char *params;
    char *trailing;

    struct ClientMessage *MyMessage;
    struct  Message368Info *MessageInfo;

    strncpy(server, "irc.koree.net", sizeof(server));
    strncpy(nick, "IRClib", sizeof(nick));
    strncpy(channel, "#irclib", sizeof(channel));

    comsocket = connect_to_server("irc.ameth.org", 6667);
    ClientSignOn(comsocket, "irclib", nick, "IRC LIB");

    i = 0;
    while(i == 0)  {
        sgetline(comsocket, buffer);
        MyMessage = ClientParseMessage(buffer);
        ClientJoinChannel(comsocket, channel, "");
        ClientSetChannelMode(comsocket, channel, "+b", "");
        if(strncmp(MyMessage->command, "368", 3) == 0)  {
            MessageInfo = (struct Message368Info *)malloc(sizeof(struct Message368Info));
            Mytrimcrlf(MyMessage->params);
            ClientParse368(MyMessage->prefix, MyMessage->command, MyMessage->params, MessageInfo);

            printf(BOLDYELLOW "ClientParse368() Tests\n" RESET);

            printf("    ClientParse368() To Test:");
            if(strncmp(MessageInfo->To, nick, sizeof(MessageInfo->To)) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else
            {
                printf(RED "        FAIL\n" RESET);
            }

            printf("    ClientParse368() Channel Test:");
            if(strncmp(MessageInfo->Channel, channel, 7) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else  {
                printf(RED "        FAIL\n" RESET);
            }

            printf("    ClientParse368() Message Test:");
            if(strncmp(MessageInfo->Message, "End of Channel Ban List", 23) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else  {
                printf(RED "        FAIL\n" RESET);
            }

            free(MessageInfo);
            exit(0);
        }
    }

}
