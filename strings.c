#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "irclib.h"
#include "types.h"

void remove_all_chars(char* str, char c) {
    char *pr = str, *pw = str;
    while (*pr) {
        *pw = *pr++;
        pw += (*pw != c);
    }
    *pw = '\0';
}

void    Mytrimcrlf(char *s)
/*
 *  *  * removes all newlines and returns from the end of the line
 *   *   */
{
        while(*(s + strlen(s) - 1) == '\n' ||
              *(s + strlen(s) - 1) == '\r' ||
          *(s + strlen(s) - 1) ==  ' ' )
              *(s + strlen(s) - 1) = '\0';
}

