#!/bin/bash

rm build.json
echo "{" >> build.json
echo "\"state\": \"SUCCESSFUL\"," >> build.json
echo "\"key\": \"$1\"," >> build.json
echo "\"name\": \"foo\"," >> build.json
echo "\"url\": \"$2\"," >> build.json
echo "\"description\": \"$3\"" >> build.json
echo "}" >> build.json
