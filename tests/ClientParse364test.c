/*
 * Automated test for ClientParse364() function
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../irclib.h"
#include "../types.h"
#include "colors.h"

void main() {
    int comsocket;
    int i;

    char buffer[255];
    char response[255];
    char server[255];
    char nick[255];

    char *prefix;
    char *command;
    char *params;
    char *trailing;

    struct ClientMessage *MyMessage;
    struct  Message364Info *MessageInfo;

    strncpy(server, "irc.koree.net", sizeof(server));
    strncpy(nick, "IRClib", sizeof(nick));

    comsocket = connect_to_server("irc.ameth.org", 6667);
    ClientSignOn(comsocket, "irclib", nick, "IRC LIB");

    i = 0;
    while(i == 0)  {
        sgetline(comsocket, buffer);
        MyMessage = ClientParseMessage(buffer);
        ClientSendLINKS(comsocket, "", "");
        if(strncmp(MyMessage->command, "364", 3) == 0)  {
            MessageInfo = (struct Message364Info *)malloc(sizeof(struct Message364Info));
            Mytrimcrlf(MyMessage->params);
            ClientParse364(MyMessage->prefix, MyMessage->command, MyMessage->params, MessageInfo);

            printf(BOLDYELLOW "ClientParse364() Tests\n" RESET);

            if(strncmp(MessageInfo->To, nick,  sizeof(MessageInfo->To)) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else
            {
                printf(RED "        FAIL\n" RESET);
            }

            free(MessageInfo);
            exit(0);
        } 
        if(strncmp(MyMessage->command, "365", 3) == 0)  {
            printf(YELLOW "     WARNING:  LINKS is likely disabled on this server.\n" RESET);
            exit(0);
        } 
    } 

}
