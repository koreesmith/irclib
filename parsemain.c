#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "irclib.h"
#include "types.h"

void    Mytrimcrlf(char *s)
/*
 *  * removes all newlines and returns from the end of the line
 *   */
{
        while(*(s + strlen(s) - 1) == '\n' ||
              *(s + strlen(s) - 1) == '\r' ||
          *(s + strlen(s) - 1) ==  ' ' )
              *(s + strlen(s) - 1) = '\0';
}

void main() {
	int comsocket;
	int i;

	char buffer[255];
	char response[255];

	char *prefix;
	char *command;
	char *params;
	char *trailing;
	
	struct ClientMessage *MyMessage;
	struct	Message351Info *MessageInfo;

	comsocket = connect_to_server("irc.ameth.org", 6667);
	ClientSignOn(comsocket, "irclib", "IRClib", "IRC LIB");

    i = 0;
    while(i == 0)  {
        sgetline(comsocket, buffer);
		printf("%s\n", buffer);
		MyMessage = ClientParseMessage(buffer);
        //ClientSendVERSION(comsocket, "");
        ClientJoinChannel(comsocket, "#irclib", "");
        ClientSendNAMES(comsocket, "#irclib", "");
		if(strncmp(MyMessage->command, "351", 3) == 0)  {
			MessageInfo = (struct Message351Info *)malloc(sizeof(struct Message351Info));
            Mytrimcrlf(MyMessage->params);
			ClientParse351(MyMessage->prefix, MyMessage->command, MyMessage->params, MessageInfo);
            printf("To:  %s\nVersion:  %s\nServer:  %s\nComments:  %s\n", MessageInfo->To, MessageInfo->Version, MessageInfo->Server, MessageInfo->Comments); 
			free(MessageInfo);
	    }
    }

}
