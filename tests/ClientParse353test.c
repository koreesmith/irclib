/*
 lientParse353() Nickname Test:");
            if(strncmp(MessageInfo->Nickname, nick, sizeof(MessageInfo->Nickname)) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else  {
                printf(RED "        FAIL\n" RESET);
            }

 */

//:irc.koree.net 353 IRClib * ~irclib dev.ameth.org *.undernet.org IRClib H :0 IRC LIB
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../irclib.h"
#include "../types.h"
#include "colors.h"

void main() {
    int comsocket;
    int i;
    int tmphops;

    char buffer[255];
    char response[255];
    char server[255];
    char nick[255];
    char user[255];
    char realname[255];

    char *prefix;
    char *command;
    char *params;
    char *trailing;

    struct ClientMessage *MyMessage;
    struct  Message353Info *MessageInfo;

    strncpy(server, "irc.koree.net", sizeof(server));
    strncpy(nick, "IRClib", sizeof(nick));
    strncpy(user, "irclib", sizeof(user));
    strncpy(realname, "IRC LIB", sizeof(realname));

    comsocket = connect_to_server("irc.ameth.org", 6667);
    ClientSignOn(comsocket, user, nick, realname);

    i = 0;
    while(i == 0)  {
        sgetline(comsocket, buffer);
        printf("%s\n");
        MyMessage = ClientParseMessage(buffer);

        ClientJoinChannel(comsocket, "#mytestchannel", "");
        if(strncmp(MyMessage->command, "353", 3) == 0)  {
            MessageInfo = (struct Message353Info *)malloc(sizeof(struct Message353Info));
            Mytrimcrlf(MyMessage->params);
            ClientParse353(MyMessage->prefix, MyMessage->command, MyMessage->params, MessageInfo);

            printf(BOLDYELLOW "ClientParse353() Tests\n" RESET);

            printf("    ClientParse353() To Test:");
            if(strncmp(MessageInfo->To, nick, sizeof(MessageInfo->To)) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else
            {
                printf(RED "        FAIL\n" RESET);
            }

            printf("    ClientParse353() IsSecret Test:");
            if(MessageInfo->IsSecret == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else {
                printf(RED "        FAIL\n" RESET);
            }
            
            printf("    ClientParse353() IsPrivate Test:");
            if(MessageInfo->IsPrivate == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else {
                printf(RED "        FAIL\n" RESET);
            }

            printf("    ClientParse353() IsPublic Test:");
            if(MessageInfo->IsPublic == 1)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else {
                printf(RED "        FAIL\n" RESET);
            }

            printf("    ClientParse353() Channel Test:");
            if(strncmp(MessageInfo->Channel, "#mytestchannel", sizeof(MessageInfo->To)) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else
            {
                printf(RED "        FAIL\n" RESET);
            }

            printf("    ClientParse353() Names Test:");
            if(strncmp(MessageInfo->Nicknames, "@IRClib", sizeof(MessageInfo->Nicknames)) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else
            {
                printf(RED "        FAIL\n" RESET);
            }

            free(MessageInfo);
            exit(0);
        }
    }

}
