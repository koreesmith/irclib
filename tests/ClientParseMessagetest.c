/*
 * Automated test for ClientParseMessage() function
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../irclib.h"
#include "../types.h"
#include "colors.h"

void main() {
    int comsocket;
    int i;

    char buffer[1024];
    char response[1024];
    char server[1024];
    char nick[1024];

    char *prefix;
    char *command;
    char *params;
    char *trailing;

    struct ClientMessage *MyMessage;

    strncpy(server, "irc.koree.net", sizeof(server));
    strncpy(nick, "IRClib", sizeof(nick));

    comsocket = connect_to_server("irc.ameth.org", 6667);
    ClientSignOn(comsocket, "irclib", nick, "IRC LIB");

    i = 0;
    while(i == 0)  {
        sgetline(comsocket, buffer);
        printf("%s", buffer);
        MyMessage = ClientParseMessage(buffer);
        ClientSendVERSION(comsocket, server);
        Mytrimcrlf(MyMessage->params);

        printf(BOLDYELLOW "ClientParseMessage() Tests\n" RESET);

        printf("    ClientParseMessage() Prefix Test:");
        if(strncmp(MyMessage->prefix, "NOTICE", sizeof(MyMessage->prefix)) == 0)  {
            printf(GREEN "      PASS\n" RESET);
        }
        else
        {
            printf(RED "        FAIL\n" RESET);
        }

        printf("    ClientParseMessage() Command Test:");
        if(strncmp(MyMessage->command, "AUTH", sizeof(MyMessage->command)) == 0)  {
            printf(GREEN "      PASS\n" RESET);
        }
        else
        {
            printf(RED "        FAIL\n" RESET);
        }

        printf("    ClientParseMessage() Params Test:");
        Mytrimcrlf(MyMessage->params);
        if(strncmp(MyMessage->params, ":", 1) == 0)  {
            printf(GREEN "      PASS\n" RESET);
        }
        else
        {
            printf(RED "        FAIL\n" RESET);
        }


        exit(0);
    }

}
