/*
 * Automated test for ClientParse315() function
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../irclib.h"
#include "../types.h"
#include "colors.h"

void main() {
    int comsocket;
    int i;

    char buffer[1024];
    char response[1024];
    char server[1024];
    char nick[1024];

    char *prefix;
    char *command;
    char *params;
    char *trailing;

    struct ClientMessage *MyMessage;
    struct  Message315Info *MessageInfo;

    strncpy(server, "irc.koree.net", sizeof(server));
    strncpy(nick, "IRClib", sizeof(nick));

    comsocket = connect_to_server("irc.ameth.org", 6667);
    ClientSignOn(comsocket, "irclib", nick, "IRC LIB");

    i = 0;
    while(i == 0)  {
        sgetline(comsocket, buffer);
        printf("Buffer:  %s\n", buffer);
        MyMessage = ClientParseMessage(buffer);
        ClientSendWHO(comsocket, nick, "");
        if(strncmp(MyMessage->command, "315", 3) == 0)  {
            MessageInfo = (struct Message315Info *)malloc(sizeof(struct Message315Info));
            Mytrimcrlf(MyMessage->params);
            ClientParse315(MyMessage->prefix, MyMessage->command, MyMessage->params, MessageInfo);

            printf(BOLDYELLOW "ClientParse315() Tests\n" RESET);

            printf("    ClientParse315() To Test:");
            if(strncmp(MessageInfo->To, nick, sizeof(MessageInfo->To)) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else
            {
                printf(RED "        FAIL\n" RESET);
            }

            printf("    ClientParse315() Nickname Test:");
            if(strncmp(MessageInfo->Nickname, nick, 15) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else  {
                printf(RED "        FAIL\n" RESET);
                exit(1);
            }

            printf("    ClientParse315() Message Test:");
            if(strncmp(MessageInfo->Message, "End of /WHO list.", 15) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else  {
                printf(RED "        FAIL\n" RESET);
            }

            free(MessageInfo);
            exit(0);
        }
    }

}
