all:
	gcc -g -w -c parse.c connect.c init.c send.c checks.c strings.c
	ar rcsv libirclib.a parse.o connect.o init.o send.o checks.o strings.o

clean:
	rm -f ./libirclib.a
	rm -f ./parsetest
	rm -f ./test
	rm -f ./*.o
test:
	gcc -g -w -o test main.c -L. -lirclib 

parsetest:
	rm -f ./parsetest
	rm -f libirclib.a
	make all
	gcc -g -w -o parsetest  parsemain.c -L. -lirclib
