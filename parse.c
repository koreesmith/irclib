#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "irclib.h"
#include "types.h"

//:irc.koree.net 403 IRClib #foo :No such channel

struct ClientMessage *ClientParseMessage(char *message)
{
	struct ClientMessage *ClientParsedMessage;
	char *tmpbuffer;
	
	ClientParsedMessage = (struct ClientMessage *)malloc(sizeof(struct ClientMessage));

        tmpbuffer = strtok(message, " ");
	strcpy(ClientParsedMessage->prefix, tmpbuffer);

	tmpbuffer = strtok(NULL, " ");
	strcpy(ClientParsedMessage->command, tmpbuffer);

	tmpbuffer = strtok(NULL, "\0");
	if(tmpbuffer == NULL)  {
		//printf("Prefix:  %s\nCommand:  %s\nParams:  %s\n", ClientParsedMessage->prefix, ClientParsedMessage->command, ClientParsedMessage->params);
		return(ClientParsedMessage);
	}
	else  {
		strcpy(ClientParsedMessage->params, tmpbuffer);
	}


	//printf("Prefix:  %s\nCommand:  %s\nParams:  %s\n", ClientParsedMessage->prefix, ClientParsedMessage->command, ClientParsedMessage->params);
	return(ClientParsedMessage);
}

int ClientParse001(char *prefix, char *command, char *params, struct  Message001Info *MessageInfo)
{
    char    *tmpbuffer;
    
    tmpbuffer = rindex(params, ' ');
    strncpy(MessageInfo->Hostmask, tmpbuffer, sizeof(MessageInfo->Hostmask));

    tmpbuffer = strtok(params, " ");
	strcpy(MessageInfo->To, tmpbuffer); 

	tmpbuffer = strtok(NULL, "\0,");
    strcpy(MessageInfo->Message, tmpbuffer);

    return(0);
}

int ClientParse002(char *prefix, char *command, char *params, struct Message002Info *MessageInfo)
{
	char	*tmpbuffer;
	int	counter = 1;

	tmpbuffer = strtok(params, ":,");
	strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

	while(tmpbuffer != NULL)  {
		tmpbuffer = strtok(NULL, ":,");
		if(counter == 1)  {
			tmpbuffer = rindex(tmpbuffer, ' ');
			tmpbuffer++;
			strncpy(MessageInfo->Servername, tmpbuffer, sizeof(MessageInfo->Servername));
		}
		if(counter == 2)  {
			tmpbuffer = rindex(tmpbuffer, ' ');
			tmpbuffer++;
			strncpy(MessageInfo->Version, tmpbuffer, sizeof(MessageInfo->Version));
		}	
		counter++;
	}
    return(0);
}

int ClientParse003(char *prefix, char *command, char *params, struct Message003Info *MessageInfo)
{
    char    *tmpbuffer;
    int     counter = 1;

    tmpbuffer = strtok(params, " ");
    strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

	tmpbuffer = strtok(NULL, "\0");
	tmpbuffer = strstr(tmpbuffer, "created"); 
	tmpbuffer = strtok(tmpbuffer, " ");
	tmpbuffer = strtok(NULL, "\0");
	strncpy(MessageInfo->CreationDate, tmpbuffer, sizeof(MessageInfo->CreationDate));	

	return(0);
}

int ClientParse004(char *prefix, char *command, char *params, struct Message004Info *MessageInfo)
{
    char    *tmpbuffer;
    int     counter = 1;

	tmpbuffer = strtok(params, " ");
    strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

	tmpbuffer = strtok(NULL, " ");
	strncpy(MessageInfo->ServerName, tmpbuffer, sizeof(MessageInfo->ServerName));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->ServerVersion, tmpbuffer, sizeof(MessageInfo->ServerVersion));

	tmpbuffer = strtok(NULL, " ");
	strncpy(MessageInfo->AvailableUserModes, tmpbuffer, sizeof(MessageInfo->AvailableUserModes));

    tmpbuffer = strtok(NULL, "\0");
    strncpy(MessageInfo->AvailableChannelModes, tmpbuffer, sizeof(MessageInfo->AvailableChannelModes));

    return(0);
}

/*
 * int *ClientParse005(char *prefix, char *command, char *params)
 *
 * This function parses a 005 message from the server.  This function is actually NOT RFC 2812 compliant, as it appears IRC servers are not using this 
 * message in the way indicated in the RFC.  Instead, this is showing a bunch of compile-time settings for the server instead.
 *
 * ex. ":irc.koree.net 005 IRClib WHOX WALLCHOPS WALLVOICES USERIP CPRIVMSG CNOTICE SILENCE=25 MODES=6 MAXCHANNELS=50 MAXBANS=50 NICKLEN=31 :are supported by this server"
 */
 
int *ClientParse005(char *prefix, char *command, char *params, struct Message005Info *MessageInfo)
{
	char	*tmpbuffer;

	tmpbuffer = strtok(params, " ");
	strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));
	
	tmpbuffer = strtok(NULL, "\0");
	strncpy(MessageInfo->ServerConfig, tmpbuffer, sizeof(MessageInfo->ServerConfig));

	return(0);
}

int ClientParse301(char *prefix, char *command, char *params, struct Message301Info *MessageInfo)
{
	char	*tmpbuffer;

	tmpbuffer = strtok(params, " ");
	strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

	tmpbuffer = strtok(NULL, " ");
	strncpy(MessageInfo->Nickname, tmpbuffer, sizeof(MessageInfo->Nickname));

	return(0);
}

int ClientParse302(char *prefix, char *command, char *params, struct Message302Info *MessageInfo)
{
	char	*tmpbuffer;

	if(strstr(params, "=-"))  {
		MessageInfo->IsAway = 1;
	}
	if(strstr(params, "=+")) {
		MessageInfo->IsAway = 0;
	}

	if(strstr(params, "*="))  {
                MessageInfo->IsIRCOper = 1;
        }

	tmpbuffer = strtok(params, " ");
	strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

	tmpbuffer = strtok(NULL, "=");
	tmpbuffer++;
	if(strstr(tmpbuffer, "*"))  {
		remove_all_chars(tmpbuffer, '*');
	}
	strncpy(MessageInfo->Nickname, tmpbuffer, sizeof(MessageInfo->Nickname));

	tmpbuffer = strtok(NULL, "\0");
	tmpbuffer++;
	strncpy(MessageInfo->Hostmask, tmpbuffer, sizeof(MessageInfo->Hostmask));

	return(0);
}

int ClientParse303(char *prefix, char *command, char *params, struct Message303Info *MessageInfo)
{
	char	*tmpbuffer;

	tmpbuffer = strtok(params, ":");
	strncpy(MessageInfo->To,  tmpbuffer, sizeof(MessageInfo->To));

	tmpbuffer = strtok(NULL, "\0");
	if(tmpbuffer == NULL)  { 
		strncpy(MessageInfo->Nickname, "", sizeof(MessageInfo->Nickname));
	}
	else  {
		strncpy(MessageInfo->Nickname, tmpbuffer, sizeof(MessageInfo->Nickname));
	}
	return(0);
}

int ClientParse305(char *prefix, char *command, char *params, struct Message305Info *MessageInfo)
{
	char	*tmpbuffer;

	tmpbuffer = strtok(params, " ");
	strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));
	
	tmpbuffer = strtok(NULL, ":");
	strncpy(MessageInfo->Reply, tmpbuffer, sizeof(MessageInfo->Reply));

	return(0);
	
}

int ClientParse306(char *prefix, char *command, char *params, struct Message306Info *MessageInfo)
{
    char    *tmpbuffer;

    tmpbuffer = strtok(params, " ");
    strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

    tmpbuffer = strtok(NULL, ":");
    strncpy(MessageInfo->Reply, tmpbuffer, sizeof(MessageInfo->Reply));

    return(0);
}

int ClientParse311(char *prefix, char *command, char *params, struct Message311Info *MessageInfo)
{
	char	*tmpbuffer;

	tmpbuffer = strtok(params, " ");
	strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

	tmpbuffer = strtok(NULL, " ");
	strncpy(MessageInfo->Nickname, tmpbuffer, sizeof(MessageInfo->Nickname));

        tmpbuffer = strtok(NULL, " ");
        strncpy(MessageInfo->Username, tmpbuffer, sizeof(MessageInfo->Username));

        tmpbuffer = strtok(NULL, " ");
        strncpy(MessageInfo->Hostname, tmpbuffer, sizeof(MessageInfo->Hostname));

        tmpbuffer = strtok(NULL, ":");

        tmpbuffer = strtok(NULL, "\0");
        strncpy(MessageInfo->Realname, tmpbuffer, sizeof(MessageInfo->Realname));

	return(0);
}

int ClientParse312(char *prefix, char *command, char *params, struct Message312Info *MessageInfo)
{
        char    *tmpbuffer;
	
        tmpbuffer = strtok(params, " ");
        strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

        tmpbuffer = strtok(NULL, " ");
        strncpy(MessageInfo->Nickname, tmpbuffer, sizeof(MessageInfo->Nickname));

        tmpbuffer = strtok(NULL, " ");
        strncpy(MessageInfo->ServerName, tmpbuffer, sizeof(MessageInfo->ServerName));

        tmpbuffer = strtok(NULL, "\0");
	tmpbuffer++;
        strncpy(MessageInfo->ServerInfo, tmpbuffer, sizeof(MessageInfo->ServerInfo));

        return(0);
}

int ClientParse313(char *prefix, char *command, char *params, struct Message313Info *MessageInfo)
{
    char    *tmpbuffer;

	tmpbuffer = strtok(params, " ");
	strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

	tmpbuffer = strtok(NULL, " ");
	strncpy(MessageInfo->Nickname, tmpbuffer, sizeof(MessageInfo->Nickname));

	tmpbuffer = strtok(NULL, "\0");
	tmpbuffer++;
	strncpy(MessageInfo->Reply, tmpbuffer, sizeof(MessageInfo->Reply));

	return(0);
}

int ClientParse317(char *prefix, char *command, char *params, struct Message317Info *MessageInfo)
{
	char	*tmpbuffer;
	int	tmpseconds;

	tmpbuffer = strtok(params, " ");
	strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

	tmpbuffer = strtok(NULL, " ");
	strncpy(MessageInfo->Nickname, tmpbuffer, sizeof(MessageInfo->Nickname));

	tmpbuffer = strtok(NULL, " ");
	tmpseconds = atoi(tmpbuffer);
	MessageInfo->SecondsIdle = tmpseconds;

	tmpbuffer = strtok(NULL, "\0");
	tmpbuffer++;
	strncpy(MessageInfo->Reply, tmpbuffer, sizeof(MessageInfo->Reply));

	return(0);
}

int ClientParse318(char *prefix, char *command, char *params, struct Message318Info *MessageInfo)
{
	char	*tmpbuffer;

	tmpbuffer = strtok(params, " ");
	strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

	tmpbuffer = strtok(NULL, " ");
	strncpy(MessageInfo->Nickname, tmpbuffer, sizeof(MessageInfo->Nickname));

	tmpbuffer = strtok(NULL, "\0");
	tmpbuffer++;
	strncpy(MessageInfo->Reply, tmpbuffer, sizeof(MessageInfo->Reply));
 
	return(0);
}

int ClientParse319(char *prefix, char *command, char *params, struct Message319Info *MessageInfo)
{
    char    *tmpbuffer;
	
    tmpbuffer = strtok(params, " ");
    strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

	tmpbuffer = strtok(NULL, " ");
	strncpy(MessageInfo->Nickname, tmpbuffer, sizeof(MessageInfo->Nickname));

	tmpbuffer = strtok(NULL, "\0");
	tmpbuffer++;
	strncpy(MessageInfo->ChannelList, tmpbuffer, sizeof(MessageInfo->ChannelList));

    return(0);
}

int ClientParse314(char *prefix, char *command, char *params, struct Message314Info *MessageInfo)
{       
        char    *tmpbuffer;
        
        tmpbuffer = strtok(params, " ");
        strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));
        
        tmpbuffer = strtok(NULL, " "); 
        strncpy(MessageInfo->Nickname, tmpbuffer, sizeof(MessageInfo->Nickname));
        
        tmpbuffer = strtok(NULL, " "); 
        strncpy(MessageInfo->Username, tmpbuffer, sizeof(MessageInfo->Username));
        
        tmpbuffer = strtok(NULL, " "); 
        strncpy(MessageInfo->Hostname, tmpbuffer, sizeof(MessageInfo->Hostname));
        
        tmpbuffer = strtok(NULL, ":");
        
        tmpbuffer = strtok(NULL, "\0");
        strncpy(MessageInfo->Realname, tmpbuffer, sizeof(MessageInfo->Realname));
        
        return(0);
}

int ClientParse315(char *prefix, char *command, char * params, struct Message315Info *MessageInfo)
{
    char    *tmpbuffer;

    tmpbuffer = strtok(params, " ");
    strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Nickname, tmpbuffer, sizeof(MessageInfo->Message));

    tmpbuffer = strtok(NULL, ":");
    strncpy(MessageInfo->Message, tmpbuffer, sizeof(MessageInfo->Message));

    return(0);
}

int ClientParse368(char *prefix, char *command, char *params, struct Message368Info *MessageInfo)
{
    char    *tmpbuffer;

    tmpbuffer = strtok(params, " ");
    strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Channel, tmpbuffer, sizeof(MessageInfo->Channel));

    tmpbuffer = strtok(NULL, ":");
    strncpy(MessageInfo->Message, tmpbuffer, sizeof(MessageInfo->Message));

    return(0);
}

int ClientParse369(char *prefix, char *command, char *params, struct Message369Info *MessageInfo)
{
    char    *tmpbuffer;

    tmpbuffer = strtok(params, " ");
    strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Nickname, tmpbuffer, sizeof(MessageInfo->Nickname));
	
    return(0);
}

int ClientParse371(char *prefix, char *command, char *params, struct Message371Info *MessageInfo)
{
    char    *tmpbuffer;

    tmpbuffer = strtok(params, " ");
    strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

    tmpbuffer = strtok(NULL, ":");
    strncpy(MessageInfo->Message, tmpbuffer, sizeof(MessageInfo->Message));

    return(0);
}

int	ClientParse322(char *prefix, char *command, char *params, struct Message322Info *MessageInfo)
{
	char	*tmpbuffer;
	int	tmpint;

	tmpbuffer = strtok(params, " ");
	strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

	tmpbuffer = strtok(NULL, " ");
	strncpy(MessageInfo->Channel, tmpbuffer, sizeof(MessageInfo->Channel));

	tmpbuffer = strtok(NULL, " ");
	tmpint = atoi(tmpbuffer);
	MessageInfo->VisibleUsers = tmpint;

	tmpbuffer = strtok(NULL, " ");
	tmpbuffer++;
	strncpy(MessageInfo->Topic, tmpbuffer, sizeof(MessageInfo->Topic));
	
	
}

int ClientParse323(char *prefix, char *command, char *params, struct Message323Info *MessageInfo)
{
    char    *tmpbuffer;

    tmpbuffer = strtok(params, " ");
    strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

    tmpbuffer = strtok(NULL, "\0");
    tmpbuffer++;
    strncpy(MessageInfo->Message, tmpbuffer, sizeof(MessageInfo->Message));

    return(0);
}

int ClientParse324(char *prefix, char *command, char *params, struct Message324Info *MessageInfo)
{
    char    *tmpbuffer;

    tmpbuffer = strtok(params, " ");
    strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Channel, tmpbuffer, sizeof(MessageInfo->Channel));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Mode, tmpbuffer, sizeof(MessageInfo->Mode));

    if(tmpbuffer = strtok(NULL, " "))  {
        strncpy(MessageInfo->ModeParams, tmpbuffer, sizeof(MessageInfo->ModeParams));
        return(0);
    }
    else  {
        return(0);
    }
}


int ClientParse325(char *prefix, char* command, char *params, struct Message325Info *MessageInfo)
{
    char    *tmpbuffer;

    tmpbuffer = strtok(params, " ");
    strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Channel, tmpbuffer, sizeof(MessageInfo->Channel));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Nickname, tmpbuffer, sizeof(MessageInfo->Nickname));

    return(0);
}

int ClientParse331(char *prefix, char *command, char *params, struct Message331Info *MessageInfo)
{
    char    *tmpbuffer;
    
    tmpbuffer = strtok(params, " ");
    strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Channel, tmpbuffer, sizeof(MessageInfo->Channel));

    tmpbuffer = strtok(NULL, "\0");
    tmpbuffer++;
    strncpy(MessageInfo->Message, tmpbuffer, sizeof(MessageInfo->Message));

    return(0);
}

int ClientParse332(char *prefix, char *command, char *params, struct Message332Info *MessageInfo)
{
    char    *tmpbuffer;

    tmpbuffer = strtok(params,  " ");
    strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Channel, tmpbuffer, sizeof(MessageInfo->Channel));

    tmpbuffer = strtok(NULL, "\0");
    tmpbuffer++;
    strncpy(MessageInfo->Topic, tmpbuffer, sizeof(MessageInfo->Topic));

    return(0);
}

int ClientParse341(char *prefix, char *command, char *params, struct Message341Info *MessageInfo)
{
    char    *tmpbuffer;

    tmpbuffer = strtok(params, " ");
    strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Channel, tmpbuffer, sizeof(MessageInfo->Channel));

    tmpbuffer = strtok(NULL, "\0");
    strncpy(MessageInfo->Nickname, tmpbuffer, sizeof(MessageInfo->Nickname));

    return(0);

}

int ClientParse342(char *prefix, char *command, char *params, struct Message342Info *MessageInfo)
{

    char    *tmpbuffer;

    tmpbuffer = strtok(params, " ");
    strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Nickname, tmpbuffer, sizeof(MessageInfo->Nickname));

    tmpbuffer = strtok(NULL, "\0");
    strncpy(MessageInfo->Message, tmpbuffer, sizeof(MessageInfo->Message));

    return(0);    
}

int ClientParse346(char *prefix, char *command, char *params, struct Message346Info *MessageInfo)
{
    char    *tmpbuffer;

    tmpbuffer = strtok(params, " ");
    strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Channel, tmpbuffer, sizeof(MessageInfo->Channel));

    tmpbuffer = strtok(NULL, "\0");
    strncpy(MessageInfo->InviteMask, tmpbuffer, sizeof(MessageInfo->InviteMask));

    return(0);
}

int ClientParse347(char *prefix, char * command, char *params, struct Message347Info *MessageInfo)
{
    char    *tmpbuffer;
    
    tmpbuffer = strtok(params, " ");
    strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));    

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Channel, tmpbuffer, sizeof(MessageInfo->Channel));

    tmpbuffer = strtok(NULL, "\0");
    strncpy(MessageInfo->Message, tmpbuffer, sizeof(MessageInfo->Message));

    return(0);
}

int ClientParse348(char *prefix, char *command, char *params, struct Message348Info *MessageInfo)
{
    char    *tmpbuffer;
    
    tmpbuffer = strtok(params, " ");
    strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Channel, tmpbuffer, sizeof(MessageInfo->Channel));

    tmpbuffer = strtok(NULL, "\0");
    strncpy(MessageInfo->ExceptionMask, tmpbuffer, sizeof(MessageInfo->ExceptionMask));    

    return(0);
}

int ClientParse349(char *prefix, char *command, char *params, struct Message349Info *MessageInfo)
{
    char    *tmpbuffer;

    tmpbuffer = strtok(params, " ");
    strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Channel, tmpbuffer, sizeof(MessageInfo->Channel));

    tmpbuffer = strtok(NULL, "\0");
    strncpy(MessageInfo->Message, tmpbuffer, sizeof(MessageInfo->Message));

    return(0);
}

int ClientParse351(char *prefix, char *command, char *params, struct Message351Info *MessageInfo)
{
    char    *tmpbuffer;

    tmpbuffer = strtok(params, " ");
    strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Version, tmpbuffer, sizeof(MessageInfo->Version));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Server, tmpbuffer, sizeof(MessageInfo->Server));

    tmpbuffer = strtok(NULL, "\0");
    tmpbuffer++;
    strncpy(MessageInfo->Comments, tmpbuffer, sizeof(MessageInfo->Comments));

    return(0);
}

int ClientParse352(char *prefix, char *command, char *params, struct Message352Info *MessageInfo)
{
    char    *tmpbuffer;

    tmpbuffer = strtok(params, " ");
    strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Channel, tmpbuffer, sizeof(MessageInfo->Channel));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->User, tmpbuffer, sizeof(MessageInfo->User));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Host, tmpbuffer, sizeof(MessageInfo->Host));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Server, tmpbuffer, sizeof(MessageInfo->Server));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Nickname, tmpbuffer, sizeof(MessageInfo->Nickname));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Stats, tmpbuffer, sizeof(MessageInfo->Stats));
    
    tmpbuffer = strtok(NULL, " ");
    tmpbuffer++;
    strncpy(MessageInfo->HopCount, tmpbuffer, sizeof(MessageInfo->HopCount));

    tmpbuffer = strtok(NULL, "\0");
    strncpy(MessageInfo->RealName, tmpbuffer, sizeof(MessageInfo->RealName));

    return(0);
}

//:irc.koree.net 353 IRClib = #irclib :IRClib Baka @KoReE
//
int ClientParse353(char *prefix, char *command, char *params, struct Message353Info *MessageInfo)
{

    char    *tmpbuffer;
    MessageInfo->IsPublic = 0;
    MessageInfo->IsPrivate = 0;
    MessageInfo->IsSecret = 0;

    tmpbuffer = strtok(params, " ");
    strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

    tmpbuffer = strtok(NULL, " ");
    if(strncmp(tmpbuffer, "=", sizeof(tmpbuffer)) == 0)  {
        MessageInfo->IsPublic = 1;
    }
    else if(strncmp(tmpbuffer, "*", sizeof(tmpbuffer)) == 0)  {
        MessageInfo->IsPrivate = 1;
    }
    else if(strncmp(tmpbuffer, "@", sizeof(tmpbuffer)) == 0)  {
        MessageInfo->IsSecret = 1;
    }
    
    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Channel, tmpbuffer, sizeof(MessageInfo->Channel));

    tmpbuffer = strtok(NULL, "\0");
    tmpbuffer++;
    strncpy(MessageInfo->Nicknames, tmpbuffer, sizeof(MessageInfo->Nicknames)); 

}

int ClientParse364(char *prefix, char *command, char *params, struct Message364Info *MessageInfo)
{
    char    *tmpbuffer;

    tmpbuffer = strtok(params, " ");
    strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Mask, tmpbuffer, sizeof(MessageInfo->Mask));

    tmpbuffer = strtok(NULL, ":");
    tmpbuffer++;
    strncpy(MessageInfo->HopCount, tmpbuffer, sizeof(MessageInfo->HopCount));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->ServerInfo, tmpbuffer, sizeof(MessageInfo->ServerInfo));
    
}

int ClientParse365(char *prefix, char *command, char *params, struct Message365Info *MessageInfo)
{
    char    *tmpbuffer;

    tmpbuffer = strtok(params, " ");
    strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Mask, tmpbuffer, sizeof(MessageInfo->Mask));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Message, tmpbuffer, sizeof(MessageInfo->Message));
}

int ClientParse366(char *prefix, char *command, char *params, struct Message366Info *MessageInfo)
{

    char    *tmpbuffer;

    tmpbuffer = strtok(params, " ");
    strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Channel, tmpbuffer, sizeof(MessageInfo->Channel));

    tmpbuffer = strtok(NULL, "\0");
    tmpbuffer++;
    strncpy(MessageInfo->Message, tmpbuffer, sizeof(MessageInfo->Message));

}

int ClientParse367(char *prefix, char *command, char *params, struct Message367Info *MessageInfo)
{
    char    *tmpbuffer;
    
    tmpbuffer = strtok(params, " ");
    strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));
    
    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->Channel, tmpbuffer, sizeof(MessageInfo->Channel));
    
    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->BanMask, tmpbuffer, sizeof(MessageInfo->BanMask));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->By, tmpbuffer, sizeof(MessageInfo->By));

    tmpbuffer = strtok(NULL, " ");
    strncpy(MessageInfo->TimeStamp, tmpbuffer, sizeof(MessageInfo->TimeStamp));
}

int ClientParse374(char *prefix, char *command, char *params, struct Message374Info *MessageInfo)
{
	char	*tmpbuffer;

	tmpbuffer = strtok(params, " ");
    	strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

	tmpbuffer = strtok(NULL, ":");
	strncpy(MessageInfo->Message, tmpbuffer, sizeof(MessageInfo->Message));
}

int ClientParse375(char *prefix, char *command, char *params, struct Message375Info *MessageInfo)
{
        char    *tmpbuffer;

        tmpbuffer = strtok(params, " ");
        strncpy(MessageInfo->To, tmpbuffer, sizeof(MessageInfo->To));

        tmpbuffer = strtok(NULL, ":");
        strncpy(MessageInfo->Message, tmpbuffer, sizeof(MessageInfo->Message));
}

struct	ClientInfo *ClientParseWHOIS(char *message)
{
	struct	ClientInfo *MyClientInfo;

	MyClientInfo = (struct ClientInfo *)malloc(sizeof(struct ClientInfo));
}
