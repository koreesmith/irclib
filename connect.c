/*
File:  connect.c
*/

#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/fcntl.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
#include "irclib.h"

int seof(int fd, int delay)
{
        fd_set myset;
        struct timeval mytime;

        FD_ZERO(&myset);
        FD_SET(fd,&myset);
        mytime.tv_sec = delay;
        if (delay==0) mytime.tv_usec = 1;
        else mytime.tv_usec=0;
        return(!(select(FD_SETSIZE, &myset, (fd_set *) NULL, (fd_set *) NULL,
	       &mytime)==1));
}

int sputline(int comsocket, char * buffer)
{
        write(comsocket,buffer,strlen(buffer));
        return(0);
}

int sgetline(int comsocket, char * buffer)
{
        int count=0;
        int index;
        *buffer=0;
        while (((char *) strstr (buffer,"\r\n")==NULL) && 
	       ((char *) strstr(buffer,"\n")==NULL) )  {
                if (seof(comsocket, 0) == 0 || 1)
                        index=read(comsocket,buffer+sizeof(char)*count,1);
                else
                        index=0;
                if (index>0)
                        count+=index;
                else
                        return(1);
                buffer[count]=0;
        }
        return(0);
}

/*
Function:  int connect_to_server();
*/
int connect_to_server(char *servername, int port)
{
	int 	socketfd;
	struct 	sockaddr_in cominfo;
	struct 	hostent *hostinfo;

	memset(&cominfo, '0', sizeof(cominfo)); 	

	hostinfo = gethostbyname(servername);
        socketfd = socket(AF_INET, SOCK_STREAM, 0);
        if (socketfd < 0)  {
                perror("Unable to create socket: ");
        }
        cominfo.sin_family = AF_INET;
        cominfo.sin_port = htons(port);
        memcpy((char *)&cominfo.sin_addr.s_addr, hostinfo->h_addr, hostinfo->h_length);
        if (connect(socketfd,(struct sockaddr *) &cominfo, sizeof(cominfo))
            < 0)  {
                perror("Unable to connect: ");
        }
	return(socketfd);
}
