/* checks.c
*/

#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/fcntl.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
#include "irclib.h"
#include "types.h"

/*
 * int ChannelExists(int comsocket, char *channelname)
 * Returns 0 if channel exists, 1 if it does not.
 */

int ChannelExists(int comsocket, char *channelname)
{
	char buffer[250];
	int i = 0;
	struct ClientMessage *myClientMessage;

	ClientSetChannelMode(comsocket, channelname, "", "");
	sgetline(comsocket, buffer);
	myClientMessage = ClientParseMessage(buffer);

	if(strncmp(myClientMessage->command, "403", 3) == 0)  {
		return(1);
	}
	else  {
		return(0);
	}

}
