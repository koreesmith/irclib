/*
 * Automated test for ClientParse367() function
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../irclib.h"
#include "../types.h"
#include "colors.h"

void main() {
    int comsocket;
    int i;

    char buffer[255];
    char response[255];
    char server[255];
    char nick[255];

    char *prefix;
    char *command;
    char *params;
    char *trailing;

    struct ClientMessage *MyMessage;
    struct  Message367Info *MessageInfo;

    strncpy(server, "irc.koree.net", sizeof(server));
    strncpy(nick, "IRClib", sizeof(nick));

    comsocket = connect_to_server("irc.ameth.org", 6667);
    ClientSignOn(comsocket, "irclib", nick, "IRC LIB");

    i = 0;
    while(i == 0)  {
        sgetline(comsocket, buffer);
        MyMessage = ClientParseMessage(buffer);
        ClientJoinChannel(comsocket, "#irclibfoobar", "");
        ClientSetChannelMode(comsocket, "#irclibfoobar", "+b", "*!*@ameth.test");
        ClientSetChannelMode(comsocket, "#irclibfoobar", "+b", "");
        if(strncmp(MyMessage->command, "367", 3) == 0)  {
            MessageInfo = (struct Message367Info *)malloc(sizeof(struct Message367Info));
            Mytrimcrlf(MyMessage->params);
            ClientParse367(MyMessage->prefix, MyMessage->command, MyMessage->params, MessageInfo);

            printf(BOLDYELLOW "ClientParse367() Tests\n" RESET);

            if(strncmp(MessageInfo->BanMask, "*!*@ameth.test",  sizeof(MessageInfo->BanMask)) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else
            {
                printf(RED "        FAIL\n" RESET);
            }

            free(MessageInfo);
            exit(0);
        }
    }

}
