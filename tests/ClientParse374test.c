/*
 * Automated test for ClientParse374() function
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../irclib.h"
#include "../types.h"
#include "colors.h"

void main() {
    int comsocket;
    int i;

    char buffer[255];
    char response[255];
    char server[255];
    char nick[255];

    char *prefix;
    char *command;
    char *params;
    char *trailing;

    struct ClientMessage *MyMessage;
    struct  Message374Info *MessageInfo;

    strncpy(server, "irc.koree.net", sizeof(server));
    strncpy(nick, "IRClib", sizeof(nick));

    comsocket = connect_to_server("irc.ameth.org", 6667);
    ClientSignOn(comsocket, "irclib", nick, "IRC LIB");

    i = 0;
    while(i == 0)  {
        sgetline(comsocket, buffer);
        printf("Buffer:  %s\n", buffer);
        MyMessage = ClientParseMessage(buffer);
	ClientSendINFO(comsocket, "irc.koree.net");
        if(strncmp(MyMessage->command, "374", 3) == 0)  {
            MessageInfo = (struct Message374Info *)malloc(sizeof(struct Message374Info));
            Mytrimcrlf(MyMessage->params);
            ClientParse374(MyMessage->prefix, MyMessage->command, MyMessage->params, MessageInfo);

            printf(BOLDYELLOW "ClientParse374() Tests\n" RESET);

            printf("    ClientParse374() To Test:");
            if(strncmp(MessageInfo->To, nick, sizeof(MessageInfo->To)) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else
            {
                printf(RED "        FAIL\n" RESET);
		exit(1);
            }

            printf("    ClientParse374() Message Test:");
            if(strncmp(MessageInfo->Message, "End of INFO list", 15) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else  {
                printf(RED "        FAIL\n" RESET);
		exit(1);
            }

            free(MessageInfo);
            exit(0);
        }
    }

}
