/*
 * Automated test for ClientParse375() function
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../irclib.h"
#include "../types.h"
#include "colors.h"

void main() {
    int comsocket;
    int i;

    char buffer[255];
    char response[255];
    char server[255];
    char nick[255];

    char *prefix;
    char *command;
    char *params;
    char *trailing;

    struct ClientMessage *MyMessage;
    struct  Message375Info *MessageInfo;

    strncpy(server, "irc.koree.net", sizeof(server));
    strncpy(nick, "IRClib", sizeof(nick));

    i = 0;
    while(i == 0)  {
	strncpy(buffer, "\":irc.koree.net 251 IRClib :- <server> Message of the day - \"", sizeof(buffer));
        MyMessage = ClientParseMessage(buffer);
        MessageInfo = (struct Message375Info *)malloc(sizeof(struct Message375Info));
        Mytrimcrlf(MyMessage->params);
        ClientParse375(MyMessage->prefix, MyMessage->command, MyMessage->params, MessageInfo);

            printf(BOLDYELLOW "ClientParse375() Tests\n" RESET);

            printf("    ClientParse375() To Test:");
            if(strncmp(MessageInfo->To, nick, sizeof(MessageInfo->To)) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else
            {
                printf(RED "        FAIL\n" RESET);
		exit(1);
            }

            printf("    ClientParse375() Message Test:");
            if(strncmp(MessageInfo->Message, "- <server> Message of the day - ", 32) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else  {
                printf(RED "        FAIL\n" RESET);
		exit(1);
            }

            free(MessageInfo);
            exit(0);
    }

}
