/*
 * Automated test for ClientParse351() function
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../irclib.h"
#include "../types.h"
#include "colors.h"

void main() {
    int comsocket;
    int i;

    char buffer[1024];
    char response[1024];
    char server[1024];
    char nick[1024];

    char *prefix;
    char *command;
    char *params;
    char *trailing;

    struct ClientMessage *MyMessage;
    struct  Message351Info *MessageInfo;

    strncpy(server, "irc.koree.net", sizeof(server));
    strncpy(nick, "IRClib", sizeof(nick));

    comsocket = connect_to_server("irc.ameth.org", 6667);
    ClientSignOn(comsocket, "irclib", nick, "IRC LIB");

    i = 0;
    while(i == 0)  {
        sgetline(comsocket, buffer);
        MyMessage = ClientParseMessage(buffer);
        ClientSendVERSION(comsocket, server);
        if(strncmp(MyMessage->command, "351", 3) == 0)  {
            MessageInfo = (struct Message351Info *)malloc(sizeof(struct Message351Info));
            Mytrimcrlf(MyMessage->params);
            ClientParse351(MyMessage->prefix, MyMessage->command, MyMessage->params, MessageInfo);

            printf(BOLDYELLOW "ClientParse351() Tests\n" RESET);

            printf("    ClientParse351() To Test:");
            if(strncmp(MessageInfo->To, nick, sizeof(MessageInfo->To)) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else
            {
                printf(RED "        FAIL\n" RESET);
            }

            printf("    ClientParse351() Server Test:");
            if(strncmp(MessageInfo->Server, server, sizeof(MessageInfo->Server)) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else  {
                printf("        FAIL\n");
            }

            printf("    ClientParse351() Version Test:");
            if(strncmp(MessageInfo->Version, "u2.1", 4) == 0)  {
                printf(GREEN "      PASS\n" RESET);
            }
            else  {
                printf(RED "        FAIL\n" RESET);
            }

            free(MessageInfo);
            exit(0);
        }
    }

}
